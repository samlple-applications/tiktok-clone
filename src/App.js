import React from "react";
import "./App.css";
import Video from "./components/video/Video";

function App() {
  return (
    <div className="app">
      <div class="app__videos">
        <Video
          url={
            "https://v16m.tiktokcdn.com/0cc652f788552a006fcc4d6327605cc8/5f576987/video/tos/useast2a/tos-useast2a-ve-0068c004/c0f305e926cf451cac7e8b9e7ae97017/?a=1233&br=4780&bt=2390&cr=0&cs=0&cv=1&dr=0&ds=3&er=&l=202009080522390101152290691C041286&lr=tiktok_m&mime_type=video_mp4&qs=0&rc=MzZlO3JzPDs3djMzOTczM0ApZjRlNTY8NGU5N2ZkODNnaWdtaGdvMms0L2hfLS1fMTZzc180YDBjYGJiNjVhMDQvXy06Yw%3D%3D&vl=&vr="
          }
          channel={"mrcjmki"}
          description={"mrcjmki doing donuts"}
          song={"immigrant song"}
          likes={123}
          messages={456}
          shares={578}
        />
        <Video />
      </div>
    </div>
  );
}

export default App;
